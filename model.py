import keras
import os

# from matplotlib import pyplot as plt
import numpy as np

# import pandas as pd
from keras.layers import (
    Conv2D,
    Dense,
    Flatten,
    Dropout,
    BatchNormalization,
    MaxPool2D,
)


from tensorflow.keras.models import Sequential
from tensorflow.keras.preprocessing.image import img_to_array, load_img
from tensorflow.keras.utils import to_categorical
from sklearn.model_selection import train_test_split

from keras.callbacks import ModelCheckpoint, LearningRateScheduler

# from keras.models import load_model

from sklearn.metrics import confusion_matrix
import seaborn as sns
import matplotlib.pyplot as plt

epochs = 15
batch_size = 32
decay_rate = 0.1
learning_rate = 0.001
# Load and preprocess data
images = []
labels = []
size = (64, 64)
folder_path = "data/train/"

# Iterate through each subfolder in the train folder
for subfolder in os.listdir(folder_path):
    subfolder_path = os.path.join(folder_path, subfolder)
    print(subfolder)
    if os.path.isdir(subfolder_path):
        # Iterate through each file in the subfolder
        for file in os.listdir(subfolder_path):
            if file.endswith(".jpg"):
                # Extract label from subfolder name
                label = subfolder
                labels.append(label)
                # Load and preprocess image
                image = load_img(
                    os.path.join(subfolder_path, file),
                    color_mode="grayscale",
                    target_size=size,
                )
                image = img_to_array(image)
                images.append(image)

images = np.array(images) / 255.0
labels = np.array(labels)

print("Unique labels:", np.unique(labels))
# Convert labels to categorical format correctly
unique_labels = np.unique(labels)
label_mapping = {label: i for i, label in enumerate(unique_labels)}
labels_mapped = np.array([label_mapping[label] for label in labels])
print("Label mapping:", label_mapping)

# Now convert mapped labels to categorical
labels_categorical = to_categorical(labels_mapped)
print("Shape of categorical labels:", labels_categorical.shape)

# Split data into train and test sets
train_images, test_images, train_labels, test_labels = train_test_split(
    images, labels_categorical, test_size=0.2, random_state=8231
)


print("Shape of train_images:", train_images.shape)
print("Shape of train_labels:", train_labels.shape)
print("Shape of test_images:", test_images.shape)
print("Shape of test_labels:", test_labels.shape)
model = Sequential()

model.add(
    Conv2D(
        75,
        (3, 3),
        strides=1,
        padding="same",
        activation="relu",
        input_shape=(64, 64, 1),
    )
)
model.add(BatchNormalization())
model.add(MaxPool2D((2, 2), strides=2, padding="same"))
model.add(Dropout(0.3))

model.add(Conv2D(15, (4, 4), strides=1, padding="same", activation="relu"))
model.add(BatchNormalization())
model.add(MaxPool2D((4, 4), strides=4, padding="same"))
model.add(Dropout(0.3))

model.add(Conv2D(5, (4, 4), strides=1, padding="same", activation="relu"))
model.add(BatchNormalization())
model.add(MaxPool2D((4, 4), strides=4, padding="same"))
model.add(Dropout(0.3))

# model.add(Conv2D(25, (3, 3), strides=1, padding="same", activation="relu"))
# model.add(BatchNormalization())
# model.add(MaxPool2D((4, 4), strides=4, padding="same"))
# model.add(Dropout(0.3))

model.add(Flatten())
model.add(Dense(units=128, activation="relu"))
model.add(Dropout(0.3))

opt = keras.optimizers.Adam(learning_rate=learning_rate)
model.add(Dense(units=10, activation="softmax"))
model.compile(optimizer=opt, loss="categorical_crossentropy", metrics=["accuracy"])
model.summary()


print("Unique labels:", np.unique(labels))
print("Label mapping:", label_mapping)
print("Label view:", labels_categorical)
print("Shape of categorical labels:", labels_categorical.shape)
print("Shape of train_images:", train_images.shape)
print("Shape of train_labels:", train_labels.shape)
print("Shape of test_images:", test_images.shape)
print("Shape of test_labels:", test_labels.shape)
print("Data type of train_images:", train_images.dtype)
print("Data type of train_labels:", train_labels.dtype)
print("Data type of test_images:", test_images.dtype)
print("Data type of test_labels:", test_labels.dtype)

checkpoint = ModelCheckpoint(
    "new_best_model.keras",
    monitor="val_accuracy",  # You can change this metric to monitor
    save_best_only=True,  # Save only the best model
    mode="max",  # 'max' for accuracy, 'min' for loss, etc.
    verbose=1,
)


def exp_decay(epoch):
    lrate = learning_rate * np.exp(-decay_rate * epoch)
    return lrate


lr_rate = LearningRateScheduler(exp_decay)
# Train the model with the callback
history = model.fit(
    train_images,
    train_labels,
    validation_data=(test_images, test_labels),
    epochs=epochs,
    batch_size=batch_size,
    callbacks=[lr_rate, checkpoint],  # Pass the checkpoint callback
)

model = keras.models.load_model("new_best_model.keras")
# Evaluate on test set
test_loss, test_acc = model.evaluate(test_images, test_labels)
print(f"Test accuracy: {test_acc:.4f}")

# Plotting training and validation accuracy
plt.figure(figsize=(10, 5))
plt.subplot(1, 2, 1)
plt.plot(history.history["accuracy"], label="Training Accuracy")
plt.plot(history.history["val_accuracy"], label="Validation Accuracy")
plt.xlabel("Epoch")
plt.ylabel("Accuracy")
plt.grid(True, linestyle="--", color="grey")
plt.legend()

# Plotting training and validation loss
plt.subplot(1, 2, 2)
plt.plot(history.history["loss"], label="Training Loss")
plt.plot(history.history["val_loss"], label="Validation Loss")
plt.xlabel("Epoch")
plt.ylabel("Loss")
plt.grid(True, linestyle="--", color="grey")
plt.legend()

plt.tight_layout()
plt.show()

# Label names in the order of the indices
label_names = [
    label for label, index in sorted(label_mapping.items(), key=lambda x: x[1])
]
# Predict on test images
predictions = model.predict(test_images)
predicted_labels = np.argmax(predictions, axis=1)
true_labels = np.argmax(test_labels, axis=1)

# Confusion matrix
cm = confusion_matrix(true_labels, predicted_labels)
plt.figure(figsize=(10, 7))
sns.heatmap(
    cm,
    annot=True,
    fmt="d",
    cmap="Blues",
    xticklabels=label_names,
    yticklabels=label_names,
)
plt.xlabel("Predicted")
plt.ylabel("True")
plt.title("Confusion Matrix")
plt.show()
