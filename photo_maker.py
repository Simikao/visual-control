import cv2
import numpy as np


# Function to create trackbars for tuning parameters
def createTrackBar():
    # Create a named window for the trackbars
    cv2.namedWindow("trackbar")
    # Use a dictionary to store trackbar names and their creation
    trackbars = {
        "Lower Background Threshold (L)": "th_lb",
        "Upper Background Threshold (U)": "th_ub",
        "Lower Skin Threshold (L)": "lh",
        "Upper Skin Threshold (U)": "uh",
        "Lower Skin Saturation (L)": "ls",
        "Upper Skin Saturation (U)": "us",
        "Lower Skin Value (L)": "lv",
        "Upper Skin Value (U)": "uv",
    }
    # Loop through the dictionary and create trackbars for each parameter
    for name, trackbar_name in trackbars.items():
        cv2.createTrackbar(trackbar_name, "trackbar", 0, 255, empty)


# Empty function for trackbars
def empty(e):
    pass


# Function to get the values of the trackbars
def getTrackbarValues():
    lh = cv2.getTrackbarPos("lh", "trackbar")
    uh = cv2.getTrackbarPos("uh", "trackbar")
    ls = cv2.getTrackbarPos("ls", "trackbar")
    us = cv2.getTrackbarPos("us", "trackbar")
    lv = cv2.getTrackbarPos("lv", "trackbar")
    uv = cv2.getTrackbarPos("uv", "trackbar")
    l_b = np.array([lh, ls, lv])
    u_b = np.array([uh, us, uv])
    return l_b, u_b


# Function to get the threshold values from trackbars
def getThreshTrackBar():
    # Get lower and upper threshold values from trackbars
    th_lb = cv2.getTrackbarPos("th_lb", "trackbar")
    th_ub = cv2.getTrackbarPos("th_ub", "trackbar")

    # Return the lower and upper threshold values
    return th_lb, th_ub


# Function to preprocess the image
def preprocess_image(frame):
    # Get threshold values
    # th_lb, th_ub = getThreshTrackBar()
    # th_lb, th_ub = (234, 255)
    th_lb, th_ub = (115, 255)

    # Threshold the image to remove background
    mask = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    _, mask = cv2.threshold(mask, th_lb, th_ub, cv2.THRESH_BINARY_INV)
    frame = cv2.bitwise_and(frame, frame, mask=mask)

    # Convert to HSV color space for better skin detection
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # Define a kernel for morphological operations
    kernel = np.ones((21, 21), np.uint8)

    # Get HSV threshold values
    # l_b, u_b = getTrackbarValues()
    # l_b, u_b = (np.array([151, 0, 134]), np.array([255, 38, 255]))
    l_b, u_b = (np.array([0, 0, 25]), np.array([255, 250, 255]))

    # Create a mask for skin detection
    mask = cv2.inRange(frame, l_b, u_b)
    mask = cv2.dilate(mask, kernel)
    cv2.imshow("mask", mask)
    # Apply the mask to the frame
    frame = cv2.bitwise_and(frame, frame, mask=mask)
    # Convert back to BGR color space
    frame = cv2.cvtColor(frame, cv2.COLOR_HSV2BGR)
    cv2.imshow("processed", frame)

    # Preprocessing steps
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frame = cv2.GaussianBlur(frame, (15, 15), 0)
    frame = cv2.resize(frame, (64, 64))

    return frame


# Function to save the processed frame
def save_frame(frame, file_number, directory):
    file_number += 1
    file_name = directory + str(file_number) + ".jpg"
    cv2.imwrite(file_name, frame)
    print("SAVED", file_name)
    return file_number


def main():
    # Open the video capture device
    cap = cv2.VideoCapture(0)

    # Create trackbars
    # createTrackBar()

    # Initialize file number and directory
    file_number = 2137
    directory = "data/train/none/"
    # Define region of interest coordinates
    pt1 = (300, 180)
    pt2 = (550, 420)

    # Main loop
    while cap.isOpened():
        # Read a frame from the video capture device
        ret, frame = cap.read()
        if not ret:
            break

        # Flip the frame horizontally
        frame = cv2.flip(frame, 1)

        # Draw a rectangle around the region of interest
        frame = cv2.rectangle(frame, pt1, pt2, (255, 0, 0), 2)

        # Extract the region of interest
        target_frame = frame[pt1[1] : pt2[1], pt1[0] : pt2[0], :]

        # Preprocess the region of interest
        processed = preprocess_image(target_frame)

        # Save the processed frame
        file_number = save_frame(processed, file_number, directory)

        # Display the original frame
        cv2.imshow("cam", frame)

        # Check for key press and break the loop if 'q' is pressed
        if cv2.waitKey(1) & 0xFF == ord("q"):
            break

    # Release the video capture device and close all windows
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
