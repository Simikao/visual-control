import cv2
import numpy as np
from keras.models import load_model

# Load your trained model
model = load_model("best_model.keras")

# Start video capture
cap = cv2.VideoCapture(0)  # '0' is typically the default value for the primary camera

# Define your label mapping dictionary
label_mapping = {
    0: "four_fingers",
    1: "one_finger",
    2: "open_palm",
    3: "three_fingers",
    4: "two_fingers",
    5: "none",
    6: "thumb_down",
    7: "thumb_left",
    8: "thumb_right",
    9: "thumb_up",
    # Add more mappings as needed
}


# Function to create trackbars for tuning parameters
def createTrackBar():
    # Create a named window for the trackbars
    cv2.namedWindow("trackbar")
    # Use a dictionary to store trackbar names and their creation
    trackbars = {
        "Lower Background Threshold (L)": "th_lb",
        "Upper Background Threshold (U)": "th_ub",
        "Lower Skin Threshold (L)": "lh",
        "Upper Skin Threshold (U)": "uh",
        "Lower Skin Saturation (L)": "ls",
        "Upper Skin Saturation (U)": "us",
        "Lower Skin Value (L)": "lv",
        "Upper Skin Value (U)": "uv",
    }
    # Loop through the dictionary and create trackbars for each parameter
    for name, trackbar_name in trackbars.items():
        cv2.createTrackbar(trackbar_name, "trackbar", 0, 255, empty)


# Empty function for trackbars
def empty(e):
    pass


# Function to get the values of the trackbars
def getTrackbarValues():
    lh = cv2.getTrackbarPos("lh", "trackbar")
    uh = cv2.getTrackbarPos("uh", "trackbar")
    ls = cv2.getTrackbarPos("ls", "trackbar")
    us = cv2.getTrackbarPos("us", "trackbar")
    lv = cv2.getTrackbarPos("lv", "trackbar")
    uv = cv2.getTrackbarPos("uv", "trackbar")
    l_b = np.array([lh, ls, lv])
    u_b = np.array([uh, us, uv])
    return l_b, u_b


# Function to get the threshold values from trackbars
def getThreshTrackBar():
    # Get lower and upper threshold values from trackbars
    th_lb = cv2.getTrackbarPos("th_lb", "trackbar")
    th_ub = cv2.getTrackbarPos("th_ub", "trackbar")

    # Return the lower and upper threshold values
    return th_lb, th_ub


# Function to preprocess the image
def preprocess_image(frame):
    # Get threshold values
    th_lb, th_ub = getThreshTrackBar()
    # th_lb, th_ub = (234, 255)
    # th_lb, th_ub = (125, 255)

    # Threshold the image to remove background
    mask = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    _, mask = cv2.threshold(mask, th_lb, th_ub, cv2.THRESH_BINARY_INV)
    frame = cv2.bitwise_and(frame, frame, mask=mask)

    # Convert to HSV color space for better skin detection
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # Define a kernel for morphological operations
    kernel = np.ones((21, 21), np.uint8)

    # Get HSV threshold values
    l_b, u_b = getTrackbarValues()
    # l_b, u_b = (np.array([151, 0, 134]), np.array([255, 38, 255]))
    # l_b, u_b = (np.array([0, 10, 25]), np.array([255, 245, 255]))

    # Create a mask for skin detection
    mask = cv2.inRange(frame, l_b, u_b)
    mask = cv2.dilate(mask, kernel)
    cv2.imshow("mask", mask)
    # Apply the mask to the frame
    frame = cv2.bitwise_and(frame, frame, mask=mask)
    # Convert back to BGR color space
    frame = cv2.cvtColor(frame, cv2.COLOR_HSV2BGR)
    cv2.imshow("processed", frame)

    # Preprocessing steps
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow("frame", frame)
    frame = cv2.GaussianBlur(frame, (15, 15), 0)
    frame = cv2.resize(frame, (64, 64))
    frame = np.reshape(frame, (64, 64, 1))
    frame = np.expand_dims(frame, axis=0)

    return frame


if not cap.isOpened():
    print("Cannot open camera")
    exit()

createTrackBar()
# Define region of interest coordinates
pt1 = (300, 180)
pt2 = (550, 420)
while True:
    # Capture frame-by-frame
    ret, frame = cap.read()
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        break
    frame = cv2.flip(frame, 1)

    # Convert frame to grayscale
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    frame = cv2.rectangle(frame, pt1, pt2, (255, 0, 0), 2)
    # Resize frame to match model's expected input
    resized_frame = frame[pt1[1] : pt2[1], pt1[0] : pt2[0], :]
    resized_frame = preprocess_image(frame)

    # Make a prediction
    prediction = model.predict(resized_frame)
    print(prediction)
    predicted_class = np.argmax(prediction)
    confidence = np.max(prediction)

    # Get class label
    class_label = label_mapping[predicted_class]

    # Display the resulting frame with the prediction
    cv2.putText(
        frame,
        f"{class_label} {confidence*100:.2f}%",
        (50, 50),
        cv2.FONT_HERSHEY_SIMPLEX,
        1,
        (255, 255, 255),
        2,
        cv2.LINE_AA,
    )
    cv2.imshow("Gesture Recognition", frame)

    # Break the loop with 'q'
    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
