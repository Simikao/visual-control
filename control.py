import pyautogui
import cv2
import numpy as np
import keras
import time


model = keras.models.load_model("best_model.keras")


# Function to create trackbars for tuning parameters
def createTrackBar():
    # Create a named window for the trackbars
    cv2.namedWindow("trackbar")
    # Use a dictionary to store trackbar names and their creation
    trackbars = {
        "Lower Background Threshold (L)": "th_lb",
        "Upper Background Threshold (U)": "th_ub",
        "Lower Skin Threshold (L)": "lh",
        "Upper Skin Threshold (U)": "uh",
        "Lower Skin Saturation (L)": "ls",
        "Upper Skin Saturation (U)": "us",
        "Lower Skin Value (L)": "lv",
        "Upper Skin Value (U)": "uv",
    }
    # Loop through the dictionary and create trackbars for each parameter
    for name, trackbar_name in trackbars.items():
        cv2.createTrackbar(trackbar_name, "trackbar", 0, 255, empty)


# Empty function for trackbars
def empty(e):
    pass


# Function to get the values of the trackbars
def getTrackbarValues():
    lh = cv2.getTrackbarPos("lh", "trackbar")
    uh = cv2.getTrackbarPos("uh", "trackbar")
    ls = cv2.getTrackbarPos("ls", "trackbar")
    us = cv2.getTrackbarPos("us", "trackbar")
    lv = cv2.getTrackbarPos("lv", "trackbar")
    uv = cv2.getTrackbarPos("uv", "trackbar")
    l_b = np.array([lh, ls, lv])
    u_b = np.array([uh, us, uv])
    return l_b, u_b


# Function to get the threshold values from trackbars
def getThreshTrackBar():
    # Get lower and upper threshold values from trackbars
    th_lb = cv2.getTrackbarPos("th_lb", "trackbar")
    th_ub = cv2.getTrackbarPos("th_ub", "trackbar")

    # Return the lower and upper threshold values
    return th_lb, th_ub


# Function to preprocess the image
def preprocess_image(frame):
    # Get threshold values
    # th_lb, th_ub = getThreshTrackBar()
    # th_lb, th_ub = (234, 255)
    th_lb, th_ub = (125, 255)

    # Threshold the image to remove background
    mask = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    _, mask = cv2.threshold(mask, th_lb, th_ub, cv2.THRESH_BINARY_INV)
    frame = cv2.bitwise_and(frame, frame, mask=mask)

    # Convert to HSV color space for better skin detection
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # Define a kernel for morphological operations
    kernel = np.ones((21, 21), np.uint8)

    # Get HSV threshold values
    l_b, u_b = getTrackbarValues()
    # l_b, u_b = (np.array([151, 0, 134]), np.array([255, 38, 255]))
    l_b, u_b = (np.array([0, 5, 5]), np.array([255, 245, 255]))

    # Create a mask for skin detection
    mask = cv2.inRange(frame, l_b, u_b)
    mask = cv2.dilate(mask, kernel)
    cv2.imshow("mask", mask)
    # Apply the mask to the frame
    frame = cv2.bitwise_and(frame, frame, mask=mask)
    # Convert back to BGR color space
    frame = cv2.cvtColor(frame, cv2.COLOR_HSV2BGR)
    cv2.imshow("processed", frame)

    # Preprocessing steps
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    cv2.imshow("frame", frame)
    frame = cv2.GaussianBlur(frame, (15, 15), 0)
    frame = cv2.resize(frame, (64, 64))
    frame = np.reshape(frame, (64, 64, 1))
    frame = np.expand_dims(frame, axis=0)

    return frame


def process_frame(target_frame):
    input = preprocess_image(target_frame)
    output = model.predict(input)
    confidence = np.max(output)
    print(output)
    class_result = list(output[0]).index(max(output[0]))
    print(class_result)
    return str(class_result), confidence


def executeTask(task):
    """--------------------------
        0: "four_fingers",
        1: "one_finger",
        2: "open_palm",
        3: "three_fingers",
        4: "two_fingers",
        5: "none",
        6: "thumb_down",
        7: "thumb_left",
        8: "thumb_right",
        9: "thumb_up",
    --------------------------"""
    task = int(task)

    match task:
        case 8:
            pyautogui.press("up")
        case 7:
            pyautogui.press("down")
        case 1:
            pyautogui.press("right")
        case 4:
            pyautogui.press("space")


def main():
    print("MODEL INITIALIZED")
    # Open the video capture device
    cap = cv2.VideoCapture(0)
    prev_result = -1
    result_cnt = 0
    decision_threshold = 5

    # Create trackbars
    createTrackBar()

    label_mapping = {
        0: "four_fingers",
        1: "one_finger",
        2: "open_palm",
        3: "three_fingers",
        4: "two_fingers",
        5: "none",
        6: "thumb_down",
        7: "thumb_left",
        8: "thumb_right",
        9: "thumb_up",
        # Add more mappings as needed
    }
    # Define region of interest coordinates
    pt1 = (300, 180)
    pt2 = (550, 420)

    # Main loop
    while cap.isOpened():
        # Read a frame from the video capture device
        ret, frame = cap.read()
        if not ret:
            break

        # Flip the frame horizontally
        frame = cv2.flip(frame, 1)

        # Draw a rectangle around the region of interest
        frame = cv2.rectangle(frame, pt1, pt2, (255, 0, 0), 2)

        # Extract the region of interest
        target_frame = frame[pt1[1] : pt2[1], pt1[0] : pt2[0], :]

        # Preprocess the region of interest
        result, confidence = process_frame(target_frame)
        if result == prev_result:
            result_cnt += 1
        else:
            result_cnt = 1
        prev_result = result
        if result_cnt == decision_threshold:
            print("EXECUTING TASK")
            executeTask(result)
            result_cnt = 0

        # Get class label
        class_label = label_mapping[int(result)]
        # Display the resulting frame with the prediction
        cv2.putText(
            frame,
            f"{class_label} {confidence*100:.2f}%",
            (50, 50),
            cv2.FONT_HERSHEY_SIMPLEX,
            1,
            (255, 255, 255),
            2,
            cv2.LINE_AA,
        )
        cv2.imshow("cam", frame)

        # Check for key press and break the loop if 'q' is pressed
        if cv2.waitKey(1) & 0xFF == ord("q"):
            break

    # Release the video capture device and close all windows
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    main()
