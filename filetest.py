import os
import numpy as np
from keras.utils import to_categorical

labels = []
images = []
folder_path = "data/test/"
# Iterate through each subfolder in the train folder
for subfolder in os.listdir(folder_path):
    subfolder_path = os.path.join(folder_path, subfolder)
    if os.path.isdir(subfolder_path):
        # Iterate through each file in the subfolder
        for file in os.listdir(subfolder_path):
            if file.endswith(".jpg"):
                # Extract label from subfolder name
                label = subfolder
                labels.append(label)
                # Load and preprocess image
                images.append(label)

# Convert labels to categorical format
unique_labels = np.unique(labels)
label_mapping = {label: i for i, label in enumerate(unique_labels)}
labels = np.array([label_mapping[label] for label in labels])
labels = to_categorical(labels)
print(labels)
print(images)
